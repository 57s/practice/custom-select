const sections = document.querySelectorAll('.section');

sections.forEach(section => {
	const header = section.querySelector('.section__header');
	const options = section.querySelectorAll('.section__item');
	const selected = section.querySelector('.section__selected');

	header.addEventListener('click', toggleOpen);

	options.forEach(option => {
		option.addEventListener('click', optionClickHandler);
	});

	function optionClickHandler(event) {
		selected.textContent = this.textContent;
		selected.dataset.value = this.dataset.value;

		onChange(selected.dataset.value);
		toggleOpen();
	}

	function toggleOpen() {
		section.classList.toggle('section_open');
	}
});

function onChange(value) {
	console.log(value);
}
